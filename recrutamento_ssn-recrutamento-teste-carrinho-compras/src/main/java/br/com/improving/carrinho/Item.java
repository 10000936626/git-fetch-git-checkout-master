package main.java.br.com.improving.carrinho;

import java.math.BigDecimal;

/**
 * Classe que representa um item no carrinho de compras.
 */
public class Item {
    private  Produto valProd;
    private Produto produto;
    private BigDecimal valorUnitario;
    private int quantidade;

    /**
     * Construtor da classe Item.
     *
     * @param produto
     * @param valorUnitario
     * @param quantidade
     */



    public Item(Produto produto, BigDecimal valorUnitario, int quantidade) {
        setProduto(produto);
        setValorUnitario(valorUnitario);
        setQuantidade(quantidade);
    }

    /**
     * Retorna o produto.
     *
     * @return Produto
     */

    public Produto getProduto() {
        return produto;
    }

    /**
     * Retorna o valor unitário do item.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    /**
     * Retorna a quantidade dos item.
     *
     * @return int
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * Retorna o valor total do item.
     *
     * @return BigDecimal
     */
    interface Multiplicador<F> {
        BigDecimal multiplicar(F val1, BigDecimal val2);
    }
    public BigDecimal getValorTotal( ) {
        Multiplicador<Integer> multiplicador = (val1, val2) -> val2.multiply(BigDecimal.valueOf(val1));
        return multiplicador.multiplicar(getQuantidade(), getValorUnitario());
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
